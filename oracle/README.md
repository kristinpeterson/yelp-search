### Oracle DB instance on AWS RDS<br />+ Oracle SQL*Plus Linux Client<br />+ Docker

### Table of Contents

* [Option 1: on your local machine](#option-1-on-your-local-machine)
* [Option 2: on a Google Cloud VM instance](#option-2-on-a-google-cloud-vm-instance)
* [Database Connection String](#database-connection-string)

#### Option 1: on your local machine

##### Prerequisites

* [AWS Account](https://aws.amazon.com/signup)
* [Docker](https://docker.com) installed locally - [Get Docker](https://docker.com/getdocker)

##### Steps

1. [Create Oracle database instance on Amazon RDS](https://aws.amazon.com/rds/oracle/) - **Important** record the Master Password, you'll need it later.
2. (Optional) By default the security group assigned to the RDS instance will be limited to your machine's public IP address. If you'd like to allow inbound traffic from any IP address (limited to DB port 1521) or to change the allowable IP(s), proceed with the following steps:
  1. Navigate to the RDS instance's info screen
  2. Select the Security Group labeled as `Active`
    ![Screencap](https://cl.ly/2A253P3D0F1z/AWS_RDS_dbinfo.png)
  3. On the `Inbound` tab, select `Edit`
  4. Under `Source` for the RDS entry, to allow inbound traffic from:
    1. any IP address, choose `Anywhere` in the dropdown
    2. your current public IP,  choose `My IP` in the dropdown
    3. a custom IP address, choose `Custom` in the dropdown and enter the IP
3. Using the following open source Docker container [_guywithnose/sqlplus_](https://hub.docker.com/r/guywithnose/sqlplus/), we'll connect to the AWS RDS instance using the [Oracle SQL*Plus Linux client](https://docs.oracle.com/cd/B19306_01/server.102/b14357/qstart.htm) with the following command:

  ```bash
  docker run --interactive guywithnose/sqlplus sqlplus {CONNECTION_STRING}
  ```

**Next: [Database Connection String](#database-connection-string)**

#### Option 2: on a Google Cloud VM instance

##### Prerequisites

* [AWS Account](https://aws.amazon.com)
* [Google Cloud Account](https://cloud.google.com)

##### Steps

1. [Create Oracle database instance on Amazon RDS](https://aws.amazon.com/rds/oracle/) - **Important** record the Master Password, you'll need it later.
  ![Screenshots]()
2. [Create Google Cloud VM instance on Compute Engine](https://cloud.google.com/compute/docs/instances/create-start-instance)
  * The `micro` (most inexpensive) machine type will suffice
  * The `Ubuntu 16.04` images was used for this project
3. [SSH into the VM instance](https://cloud.google.com/compute/docs/instances/connecting-to-instance) - you can do so super quickly/easily via your browser on the Google Cloud Console or configure other favorite method for SSHing
4. Install Docker by following the [instructions for Ubuntu](https://docs.docker.com/engine/installation/linux/ubuntulinux/) or use this script which is configured for Ubuntu 16.04
5. Reboot the VM instance:
  ```bash
  sudo reboot
  ```

6. Allow connection between Google Cloud VM instance and the AWS RDS Oracle DB Instance:
  1. Convert the IP address for the Google Cloud instance from Ephemeral (ie. temporary) to Static (ie. unchanging)
  ![Screencap]()
  2. On the AWS RDS Oracle instance's info screen, select the assigned **Security Group**, edit the security groups Inbound connection settings restricting access to the static IP Address of the Google Cloud VM instance.
  ![Screencap]()
7. SSH back into the VM Using the following open source Docker container [guywithnose/sqlplus](https://hub.docker.com/r/guywithnose/sqlplus/), we'll connect to the AWS RDS instance using the [Oracle SQL*Plus Linux client](https://docs.oracle.com/cd/B19306_01/server.102/b14357/qstart.htm) with the following command:

  ```bash
  docker run --interactive guywithnose/sqlplus sqlplus {CONNECTION_STRING}
  ```

**Next: [Database Connection String](#database-connection-string)**

#### Database Connection String

`dbuser/dbpassword@//dburl:dbport/dbname`

All of the variables above can be obtained from the info screen of the AWS RDS instance, except for `dbpassword` which is the Master Password provided when creating the database instance:

![AWS RDS Info Screenshot](https://cl.ly/372T3u3Y2832/AWS_RDS_dbinfo.png)
