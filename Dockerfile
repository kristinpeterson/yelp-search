FROM ubuntu:14.04

MAINTAINER kristinpeterson

ENV ORACLE_HOME /usr/lib/oracle/11.2/client64
ENV PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/bin:/usr/lib/oracle/11.2/client64/bin
ENV LD_LIBRARY_PATH /usr/lib/oracle/11.2/client64/lib/${LD_LIBRARY_PATH:+:$LD_LIBRARY_PATH}

WORKDIR /home/yelp

COPY requirements.txt /home/yelp/
COPY oracle/*.rpm /oracle/

RUN apt-get update \
    && apt-get install -y \
    		    python-pip \
    		    python-dev \
    		    build-essential \
    		    libaio1 \
    		    alien \
    && rm -rf /var/lib/apt/lists/* \
    && sudo alien -i /oracle/oracle-instantclient11.2-basic-11.2.0.4.0-1.x86_64.rpm \
    && sudo alien -i /oracle/oracle-instantclient11.2-devel-11.2.0.4.0-1.x86_64.rpm \
    && sudo alien -i /oracle/oracle-instantclient11.2-sqlplus-11.2.0.4.0-1.x86_64.rpm \
    && rm -rf /oracle \
    && sudo ldconfig \
    && pip install -r requirements.txt

WORKDIR /home/yelp/yelp_search
