function clear_ui() {
    $(".statement").empty();
    $(".table").empty();
    $(".loading").addClass("hide");
    $(".errors").addClass("hide");
    $(".results-container").addClass("hide");
    $(".breadcrumb").html("<li class=\"active\">Results</li>");
}

function prep_reviews_table() {
  $(".results-table").empty();
  $(".statement").empty();
  $(".loading").removeClass("hide");
}

function handle_error(data) {
    $(".loading").addClass("hide");
    $(".table").empty();
    $(".alert-danger").append("<strong>Error</strong> " + data["responseJSON"]["error_code"] + "  -  " + data["responseJSON"]["error_message"]);
    $(".errors").removeClass("hide");
}

function build_statement(select, from, join_conditions, conditions, and_or) {
    var limit = $(".limit").val();
    if(!limit) {
      limit = 100
    }
    window.query_limit = limit;
    var where = "WHERE ROWNUM <= " + limit;
    var pretyprint = where;
    
    for(idx in join_conditions) {
        where = where + " AND " + join_conditions[idx];
        pretyprint = pretyprint + "</br>AND " + join_conditions[idx];
    }
    for(idx in conditions) {
        where = where + " " + and_or + " " + conditions[idx];
        pretyprint = pretyprint + "</br>" + and_or + " " + conditions[idx];
    }
    $(".statement").html("<div class=\"alert alert-success alert-dismissable\" role=\"alert\">" + 
                         "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" + 
                         "<span aria-hidden=\"true\">&times;</span>" + 
                         "</button><p>" + select + "</br>" + from + "</br>" + pretyprint +"</p></div>");    
    return select + " " + from + " " + where;
}

function execute_query(table_headers, statement, linkable) {
    var start_time = new Date().getTime();
    var data = { 'statement': statement };
    $.ajax({
        type : "POST",
        url : "search",
        data: JSON.stringify(data),
        contentType: 'application/json;charset=UTF-8',
        success: function(result) {
            var request_time = new Date().getTime() - start_time;
            $(".statement").append("<div class=\"alert alert-info alert-dismissable\" role=\"alert\"><button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button><p>Query runtime: " + request_time + "ms / ~" + request_time/60000 + " minutes</p>");
            $(".loading").addClass("hide");
            $(".results-table").html(table_headers);
            for(record in result) {
                var id = result[record][0];
                if(linkable) {
                    $(".results-table").append("<tr class=\"record\" id=\"" + id + "\">");
                } else {
                    $(".results-table").append("<tr id=\"" + id + "\">");
                }
                for(attribute in result[record]) {
                    if(linkable) {
                      $("#" + id).append("<td><a href=\"#\">" + result[record][attribute] + "</a></td>");
                    } else {
                      $("#" + id).append("<td>" + result[record][attribute] + "</td>");
                    }
                }
                $(".results-table").append("</tr>");
            }
        },
        error: function (data) {
            handle_error(data);
        }
    });   
}

$(document).ready( function (){
    $(function() {
        $(".datepicker").datepicker();
    } );

});
