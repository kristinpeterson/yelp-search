$(document).ready( function (){    
    handle_categories();

    $(".businesses_nav").addClass("active");

    $(".users_nav, .navbar-brand").click(function(event) {
        $(".businesses_nav").removeClass("active");
        $(".users_nav").addClass("active");
    });

    $(".business_search_form").submit(function(event) {
        clear_ui();
        $(".search_container").addClass("hide");
        $(".results-container").removeClass("hide");
        $(".loading").removeClass("hide");  
        $(".breadcrumb").prepend("<li><a href=\"/businesses\">Businesses</a></li>");
        statement = build_business_statement();
        table_headers = "<tr><th>business_id</th><th>name</th><th>full_address</th><th>city</th><th>state</th></tr>";
        execute_query(table_headers, statement, 1);
        event.preventDefault();
    });
        
    $(document.body).on('click', '.record', function(event) {
        prep_reviews_table();
        var business_id = $( this ).attr("id");
        table_headers = "<tr><th>review_id</th><th>user_id</th><th>business_id</th><th>publish_date</th><th>stars</th><th>votes</th><th>content</th></tr>";
        statement = "SELECT DISTINCT R.review_id, R.user_id, R.business_id, R.publish_date, R.stars, R.votes, TO_CHAR(SUBSTR(R.content,0,50)) FROM Review R WHERE R.business_id = '" + business_id + "' AND ROWNUM <= " + window.query_limit;
        $(".breadcrumb").html("<li><a href=\"/users\">Businesses</a></li><li>" + business_id + "</li><li class=\"active\">Reviews</li>");
        $(".statement").html("<div class=\"alert alert-success alert-dismissable\" role=\"alert\">" + 
                             "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" + 
                             "<span aria-hidden=\"true\">&times;</span>" + 
                             "</button><p>SELECT DISTINCT R.review_id, R.user_id, R.business_id, R.publish_date, R.stars, R.votes, TO_CHAR(SUBSTR(R.content,0,50))</br>FROM Review R</br>WHERE R.business_id = '" + business_id + "'</br>AND ROWNUM <= " + window.query_limit + "</p></div>");
        execute_query(table_headers, statement, 0);
    });

});

function handle_categories() {
    $(".main_categories").append("<option disabled>Loading Main Categories...</option>");
    $(".subcategories").append("<option disabled>Loading Subcategories...</option>");
    $.ajax({
        type : "GET",
        url : "categories",
        success: function(result) {
            $(".main_categories").empty();
            $(".subcategories").empty();
            for(record in result) {
                name = result[record][0];
                type = result[record][1];
                if(type === 'main') {
                    $(".main_categories").append("<option value='" + name + "'>" + name + "</option>");
                } else {
                    $(".subcategories").append("<option value='" + name + "'>" + name + "</option>");
                }
            }
        },
        error: function (data) {
            handle_error(data);
        }
    });
}

function build_business_statement() {
    var select = "SELECT DISTINCT B.business_id, B.name, B.full_address, B.city, B.state";
    var from = "FROM Business B";
    var conditions = [];
    var join_conditions = [];

    var main_categories = [];
    var subcategories = [];
    var checkin_from_day = $(".checkin_from_day").val();
    var checkin_from_hour = $(".checkin_from_hour").val();
    var checkin_to_day = $(".checkin_to_day").val();
    var checkin_to_hour = $(".checkin_to_hour").val();
    var checkin_count_condition = $(".checkin_count_condition").val();
    var checkin_count_value = $(".checkin_count_value").val();
    var review_from = $(".review_from").val();
    var review_to = $(".review_to").val();
    var review_stars_condition = $(".review_stars_condition").val();
    var review_stars_value = $(".review_stars_value").val();
    var review_votes_condition = $(".review_votes_condition").val();
    var review_votes_value = $(".review_votes_value").val();
    var and_or = $(".business_and_or").val();

    $('.main_categories option:selected').each(function(){ main_categories.push($(this).val()); });
    $('.subcategories option:selected').each(function(){ subcategories.push($(this).val()); });
    
    if (main_categories.length > 0 || subcategories.length > 0) {
        from = from + ", BusinessCategoryMap BC";
        join_conditions.push("B.business_id = BC.business_id");
        var condition = "BC.category_name = ";
        if(and_or && and_or == "AND") {
            condition = condition + " ALL (";
        } else {
            condition = condition + " ANY (";
        }
        for(idx in main_categories) {
            condition = condition + "\'" + main_categories[idx] + "\',";
        }
        for(idx in subcategories) {
            condition = condition + "\'" + subcategories[idx] + "\',";
        }
        condition = condition.slice(0, -1) + ")";
        conditions.push(condition);
    }
    if(checkin_from_day || checkin_from_hour || checkin_to_day || checkin_to_hour) {
        from = from + ", Checkin C";
        join_conditions.push("B.business_id = C.business_id");
    }
    if(checkin_from_day) {
        conditions.push("C.day >= " + checkin_from_day);
    }
    if(checkin_from_hour) {
        conditions.push("C.hour >= " + checkin_from_hour);
    }
    if(checkin_to_day) {
        conditions.push("C.day <= " + checkin_to_day);
    }
    if(checkin_to_hour) {
        conditions.push("C.hour <= " + checkin_to_hour);
    }
    if(review_from || review_to || review_stars_value || review_votes_value) {
        from = from + ", Review R";
        join_conditions.push("B.business_id = R.business_id");
    }
    if(review_from) {
      conditions.push("R.publish_date  >= TO_DATE(\'" + review_from + "\', \'MM/DD/YYYY\')");
    }
    if(review_to) {
      conditions.push("R.publish_date  <= TO_DATE(\'" + review_to + "\', \'MM/DD/YYYY\')");
    }
    if (review_stars_value) {
        conditions.push("R.stars " + review_stars_condition + " " + review_stars_value);
    }
    if (review_votes_value) {
        conditions.push("R.votes " + review_votes_condition + " " + review_votes_value);
    }
    return build_statement(select, from, join_conditions, conditions, and_or);
}
