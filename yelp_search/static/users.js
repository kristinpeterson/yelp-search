$(document).ready( function (){
    $(".users_nav").addClass("active");

    $(".businesses_nav").click(function(event) {
        $(".businesses_nav").addClass("active");
        $(".users_nav").removeClass("active");
    });

    $(".user_search_form").submit(function(event) {
        clear_ui();
        $(".search_container").addClass("hide");
        $(".results-container").removeClass("hide");
        $(".loading").removeClass("hide");
        $(".breadcrumb").prepend("<li><a href=\"/users\">Users</a></li>");
        statement = build_user_statement();
        table_headers = "<tr><th>user_id</th><th>name</th><th>yelping_since</th><th>review_count</th><th>friend_count</th><th>average_stars</th></tr>";
        execute_query(table_headers, statement, 1);
        event.preventDefault();
    });

    $(document.body).on('click', '.record', function(event) {
        prep_reviews_table();
        var user_id = $( this ).attr("id");
        table_headers = "<tr><th>review_id</th><th>user_id</th><th>business_id</th><th>publish_date</th><th>stars</th><th>votes</th><th>content</th></tr>";
        statement = "SELECT DISTINCT R.review_id, R.user_id, R.business_id, R.publish_date, R.stars, R.votes, TO_CHAR(SUBSTR(R.content,0,50)) FROM Review R WHERE R.user_id = '" + user_id + "' AND ROWNUM <= " + window.query_limit;
        $(".breadcrumb").html("<li><a href=\"/users\">Users</a></li><li>" + user_id + "</li><li class=\"active\">Reviews</li>");
        $(".statement").html("<div class=\"alert alert-success alert-dismissable\" role=\"alert\">" + 
                             "<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">" + 
                             "<span aria-hidden=\"true\">&times;</span>" + 
                             "</button><p>SELECT DISTINCT R.review_id, R.user_id, R.business_id, R.publish_date, R.stars, R.votes, TO_CHAR(SUBSTR(R.content,0,50))</br>FROM Review R</br>WHERE R.user_id = '" + user_id + "'</br>AND ROWNUM <= " + window.query_limit + "</p></div>");
        execute_query(table_headers, statement, 0);
    });

});

function build_user_statement () {
    var select = "SELECT DISTINCT user_id, name, yelping_since, review_count, friend_count, average_stars";
    var from = "FROM YelpUser U";
    var conditions = [];

    var yelping_since = $(".yelping_since").val();
    var review_count_condition = $(".review_count_condition").val();
    var review_count_value = $(".review_count_value").val();
    var friend_count_value = $(".friend_count_value").val();
    var friend_count_condition = $(".friend_count_condition").val();
    var average_stars_value = $(".average_stars_value").val();
    var average_stars_condition = $(".average_stars_condition").val();
    var and_or = $(".user_and_or").val();

    if (yelping_since) {
        conditions.push("U.yelping_since  <= TO_DATE(\'" + yelping_since + "\', \'MM/DD/YYYY\')");
    }
    if (review_count_value) {
        conditions.push("U.review_count " + review_count_condition + " " + review_count_value);
    }
    if (friend_count_value) {
        conditions.push("U.friend_count " + friend_count_condition + " " + friend_count_value);
    }
    if (average_stars_value) {
        conditions.push("U.average_stars " + average_stars_condition + " " + average_stars_value);
    }

    return build_statement(select, from, [], conditions, and_or);
}
