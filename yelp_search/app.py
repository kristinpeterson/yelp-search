import cx_Oracle
import os
import json

from flask import Flask, request, session, g, redirect, url_for, abort, \
     render_template, flash, jsonify

app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
  DATABASE=os.getenv('DB_CONNECTION_STRING'),
  SECRET_KEY='u9/3@fh[eoBN77=pK4jP*nW$Q34b/Q=xiCMY{9XNCTvT8F;6ZE',
  USERNAME='kristinpeterson',
  PASSWORD='password'
))

def connect_db():
  con = cx_Oracle.connect(app.config['DATABASE'])
  cur = con.cursor()
  cur.arraysize = 256
  return cur

def get_db():
  if not hasattr(g, 'db'):
    g.db = connect_db()
  return g.db

@app.teardown_appcontext
def close_db(error):
  if hasattr(g, 'db'):
    g.db.close()

@app.route('/', methods=['GET', 'POST'])
def root():
  return render_template('users.html')
  
@app.route('/users', methods=['GET', 'POST'])
def users():
  return render_template('users.html')

@app.route('/businesses', methods=['GET', 'POST'])
def businesses():
  return render_template('businesses.html')

@app.route('/users/<user_id>/reviews', methods=['GET'])
def user_reviews(user_id):
  print user_id
  try:
    db = get_db()
    statement = "SELECT DISTINCT review_id, user_id, business_id, publish_date, stars, votes, TO_CHAR(SUBSTR(content,0,50)) FROM Review WHERE user_id = '" + user_id + "' AND ROWNUM <= 100"
    cur = db.execute(statement)
    return jsonify(cur.fetchall())
  except cx_Oracle.DatabaseError as dbe:
    error, = dbe.args
    error_response = { "error_code": error.code, "error_message": error.message }
    return jsonify(error_response), 500

@app.route('/businesses/<business_id>/reviews', methods=['GET'])
def business_reviews(business_id):
  print business_id
  try:
    db = get_db()
    statement = "SELECT DISTINCT review_id, user_id, business_id, publish_date, stars, votes, TO_CHAR(SUBSTR(content,0,50)) FROM Review WHERE business_id = '" + business_id + "' AND ROWNUM <= 100"
    cur = db.execute(statement)
    return jsonify(cur.fetchall())
  except cx_Oracle.DatabaseError as dbe:
    error, = dbe.args
    error_response = { "error_code": error.code, "error_message": error.message }
    return jsonify(error_response), 500

@app.route('/categories', methods=['GET'])
def categories():
  try:
    db = get_db()
    statement = "SELECT * FROM BusinessCategory"
    cur = db.execute(statement)
    return jsonify(cur.fetchall())
  except cx_Oracle.DatabaseError as dbe:
    error, = dbe.args
    error_response = { "error_code": error.code, "error_message": error.message }
    return jsonify(error_response), 500

@app.route('/search', methods=['POST'])
def execute_search():
  try:
    db = get_db()
    statement = request.get_json()['statement']
    cur = db.execute(statement)
    return jsonify(cur.fetchall())
  except cx_Oracle.DatabaseError as dbe:
    error, = dbe.args
    error_response = { "error_code": error.code, "error_message": error.message }
    return jsonify(error_response), 500

if __name__ == '__main__':
    app.run(debug=True,host='0.0.0.0')
