import cx_Oracle
import os
import json
from datetime import datetime

PAGE_LIMIT = 100000
startTime = datetime.now()

def handle_batch_errors(name):
  error_count = 0
  for error in cur.getbatcherrors():
    error_count = error_count + 1
  if error_count > 0: 
    print name + ': ' + str(error_count) + ' errors encountered'

def insert(name, statement, data):
  try:
    cur.prepare(statement)
    cur.executemany(None, data, batcherrors=True)
    con.commit()
  except Exception as e:
    print str(e) 
  handle_batch_errors(name)

print "connecting to db and opening cursor"
con = cx_Oracle.connect(os.environ['DB_CONNECTION_STRING'])
cur = con.cursor()

print "opening data files"
yelp_business = open('data/yelp_business.json', 'r')
yelp_checkin = open('data/yelp_checkin.json', 'r')
yelp_review = open('data/yelp_review.json', 'r')
yelp_user = open('data/yelp_user.json', 'r')

cat_list = [
  'Active Life',
  'Arts & Entertainment',
  'Automotive',
  'Car Rental',
  'Cafes',
  'Beauty & Spas',
  'Convenience Stores',
  'Dentists',
  'Doctors',
  'Drugstores',
  'Department Stores',
  'Education',
  'Event Planning & Services',
  'Flowers & Gifts',
  'Food',
  'Health & Medical',
  'Home Services',
  'Home & Garden',
  'Hospitals',
  'Hotels & Travel',
  'Hardware Stores',
  'Grocery',
  'Medical Centers',
  'Nurseries & Gardening',
  'Nightlife',
  'Restaurants',
  'Shopping',
  'Transportation'
]

main_categories = [
  {'1': 'Active Life'},
  {'1': 'Arts & Entertainment'},
  {'1': 'Automotive'},
  {'1': 'Car Rental'},
  {'1': 'Cafes'},
  {'1': 'Beauty & Spas'},
  {'1': 'Convenience Stores'},
  {'1': 'Dentists'},
  {'1': 'Doctors'},
  {'1': 'Drugstores'},
  {'1': 'Department Stores'},
  {'1': 'Education'},
  {'1': 'Event Planning & Services'},
  {'1': 'Flowers & Gifts'},
  {'1': 'Food'},
  {'1': 'Health & Medical'},
  {'1': 'Home Services'},
  {'1': 'Home & Garden'},
  {'1': 'Hospitals'},
  {'1': 'Hotels & Travel'},
  {'1': 'Hardware Stores'},
  {'1': 'Grocery'},
  {'1': 'Medical Centers'},
  {'1': 'Nurseries & Gardening'},
  {'1': 'Nightlife'},
  {'1': 'Restaurants'},
  {'1': 'Shopping'},
  {'1': 'Transportation'}
]

sub_categories = []

setup_statements = [
  'DROP TABLE YelpUser CASCADE CONSTRAINTS PURGE',
  'DROP TABLE BusinessCategory CASCADE CONSTRAINTS PURGE',
  'DROP TABLE Business CASCADE CONSTRAINTS PURGE',
  'DROP TABLE BusinessCategoryMap CASCADE CONSTRAINTS PURGE',
  'DROP TABLE Checkin CASCADE CONSTRAINTS PURGE',
  'DROP TABLE Review CASCADE CONSTRAINTS PURGE',
  """
  CREATE TABLE YelpUser(
    user_id VARCHAR(100),
    yelping_since DATE,
    name VARCHAR(50),
    review_count NUMBER,
    average_stars NUMBER,
    friend_count NUMBER,
    CONSTRAINT user_pk PRIMARY KEY (user_id)
  )
  """,
  """
  CREATE TABLE BusinessCategory(
    name VARCHAR(100),
    type VARCHAR(4),
    CONSTRAINT business_category_pk PRIMARY KEY (name),
    CONSTRAINT check_type CHECK(
        type IN ('main',
                 'sub')
      )
  )
  """,
  """
  CREATE TABLE Business(
    business_id VARCHAR(100),
    name VARCHAR(100),
    full_address VARCHAR(300),
    city VARCHAR(50),
    state VARCHAR(50),
    latitude NUMBER,
    longitude NUMBER,
    CONSTRAINT business_pk PRIMARY KEY (business_id)
  )
  """,
  """
  CREATE TABLE BusinessCategoryMap(
    business_id VARCHAR(100),
    category_name VARCHAR(100),
    CONSTRAINT fk_bc_map_bid
      FOREIGN KEY (business_id)
      REFERENCES Business (business_id)
      ON DELETE CASCADE,
    CONSTRAINT fk_bc_map_cat
      FOREIGN KEY (category_name)
      REFERENCES BusinessCategory (name)
      ON DELETE CASCADE,
    CONSTRAINT id_name_unique UNIQUE (business_id, category_name)
  )
  """,
  """
  CREATE TABLE Checkin(
    business_id VARCHAR(100),
    day Number,
    hour Number,
    count NUMBER,
    CONSTRAINT fk_checkin_bid
      FOREIGN KEY (business_id)
      REFERENCES Business (business_id)
      ON DELETE CASCADE
  )
  """,
  """
  CREATE TABLE Review(
    review_id VARCHAR(100) not null,
    user_id VARCHAR(100) not null,
    business_id VARCHAR(100) not null,
    publish_date Date not null,
    stars NUMBER,
    content CLOB,
    votes NUMBER,
    CONSTRAINT review_pk PRIMARY KEY (review_id),
    CONSTRAINT fk_user_review
      FOREIGN KEY (user_id)
      REFERENCES YelpUser (user_id)
      ON DELETE CASCADE,
    CONSTRAINT fk_business_review
      FOREIGN KEY (business_id)
      REFERENCES Business (business_id)
      ON DELETE CASCADE,
    CONSTRAINT check_stars
      CHECK (stars BETWEEN 1 AND 5)
  )
  """
]

#TODO: Split into delete / create
print "setting up database"
for statement in setup_statements:
  try:
    cur.execute(statement)
  except Exception as e:
    print "setup_statements"
    print str(e)
    print statement

print "inserting main categories"
statement = "INSERT INTO BusinessCategory (name, type) VALUES(:1, 'main')"
insert('main_categories', statement, main_categories)

print "processing users"
users = []
for line in yelp_user:
  user = json.loads(line)
  users.append( { '1': user['user_id'].encode('utf-8'), 
                  '2': user['yelping_since'].encode('utf-8'), 
                  '3': user['name'].encode('utf-8'),
                  '4': user['review_count'],
                  '5': user['average_stars'],
                  '6': len(user['friends']) } )
yelp_user.close() 

print "inserting users"
statement = "INSERT INTO YelpUser (user_id, yelping_since, name, review_count, average_stars, friend_count) VALUES(:1, TO_DATE(:2, 'YYYY-MM'), :3, :4, :5, :6)"
insert('users', statement, users)

print "processing businesses"
businesses = []
bc_map = []
for line in yelp_business:
  business = json.loads(line)
  businesses.append( { '1': business['business_id'].encode('utf-8'), 
                       '2': business['name'].encode('utf-8'), 
                       '3': business['full_address'].encode('utf-8'), 
                       '4': business['city'].encode('utf-8'), 
                       '5': business['state'].encode('utf-8'), 
                       '6': business['latitude'], 
                       '7': business['longitude'] } )

  for category in business['categories']:
    # BUILD BUSINESS->CATEGORY MAPPING
    bc_map.append({'1': business['business_id'], '2': category})
    # DETECT NEW SUBCATEGORIES
    if category not in cat_list:
      cat_list.append(category)
      sub_categories.append({'1': category})
yelp_business.close()

print "inserting businesses"
statement = 'INSERT INTO Business (business_id, name, full_address, city, state, latitude, longitude) VALUES(:1, :2, :3, :4, :5, :6, :7)'
insert('businesses', statement, businesses)

print "inserting reviews"
pages = {}
line_num = 1
page_num = 1
next_page = True
num_lines = sum(1 for line in open('data/yelp_review.json'))

for line in yelp_review:
  if next_page:
    page = 'reviews_page{0}'.format(page_num)
    pages[page] = []
    next_page = False
  review = json.loads(line)
  votes = int(review['votes']['useful']) + int(review['votes']['funny']) + int(review['votes']['cool'])
  pages[page].append(
    {'1': review['review_id'].encode('utf-8'),
     '2': review['user_id'].encode('utf-8'),
     '3': review['business_id'].encode('utf-8'),
     '4': review['date'].encode('utf-8'),
     '5': review['stars'],
     '6': review['text'].encode('utf-8'),
     '7': votes})

  if line_num % PAGE_LIMIT == 0 or line_num == num_lines:
    statement = "INSERT INTO Review (review_id, user_id, business_id, publish_date, stars, content, votes) VALUES(:1, :2, :3, TO_DATE(:4, 'YYYY-MM-DD'), :5, :6, :7)"
    insert(page, statement, pages[page])
    del pages[page]
    page_num = page_num + 1
    next_page = True

  line_num = line_num + 1

yelp_review.close()

print "processing checkins"
checkins = []
for raw_line in yelp_checkin:
  line = json.loads(raw_line)
  info = line['checkin_info']
  for item in info:
    count = int(info[item])
    business_id = line['business_id'].encode('utf-8')
    time_day = item.split('-')
    hour = time_day[0]
    day = time_day[1]
    checkins.append( { 
              '1': business_id, 
              '2': day, 
              '3': hour,
              '4': count } )
yelp_checkin.close()

print "inserting checkins"
statement = 'INSERT INTO Checkin (business_id, day, hour, count) VALUES(:1, :2, :3, :4)'
insert('checkins', statement, checkins)

print "inserting subcategories"
statement = 'INSERT INTO BusinessCategory (name, type) VALUES(:1, \'sub\')'
insert('subcategories', statement, sub_categories)

print "inserting business category map"
statement = 'INSERT INTO BusinessCategoryMap (business_id, category_name) VALUES(:1, :2)'
insert('bc_map', statement, bc_map)

print "closing cursor and db connection"
cur.close()
con.close()

print "\nSCRIPT RUNTIME:"
print datetime.now() - startTime
