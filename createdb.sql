/* SETTINGS */
set sqlblanklines on;

CREATE TABLE YelpUser(
  user_id VARCHAR(100),
  yelping_since DATE,
  name VARCHAR(50),
  review_count NUMBER,
  average_stars NUMBER,
  friend_count NUMBER,
  CONSTRAINT user_pk PRIMARY KEY (user_id)
);
show err;
CREATE TABLE BusinessCategory(
  name VARCHAR(100),
  type VARCHAR(4),
  CONSTRAINT business_category_pk PRIMARY KEY (name),
  CONSTRAINT check_type CHECK(
      type IN ('main',
               'sub')
    )
);
show err;
CREATE TABLE Business(
  business_id VARCHAR(100),
  name VARCHAR(100),
  full_address VARCHAR(300),
  city VARCHAR(50),
  state VARCHAR(50),
  latitude NUMBER,
  longitude NUMBER,
  CONSTRAINT business_pk PRIMARY KEY (business_id)
);
show err;
CREATE TABLE BusinessCategoryMap(
  business_id VARCHAR(100),
  category_name VARCHAR(100),
  CONSTRAINT fk_bc_map_bid
    FOREIGN KEY (business_id)
    REFERENCES Business (business_id)
    ON DELETE CASCADE,
  CONSTRAINT fk_bc_map_cat
    FOREIGN KEY (category_name)
    REFERENCES BusinessCategory (name)
    ON DELETE CASCADE,
  CONSTRAINT id_name_unique UNIQUE (business_id, category_name)
);
show err;
CREATE TABLE Checkin(
  business_id VARCHAR(100),
  day Number,
  hour Number,
  count NUMBER,
  CONSTRAINT fk_checkin_bid
    FOREIGN KEY (business_id)
    REFERENCES Business (business_id)
    ON DELETE CASCADE
);
show err;
CREATE TABLE Review(
  review_id VARCHAR(100) not null,
  user_id VARCHAR(100) not null,
  business_id VARCHAR(100) not null,
  publish_date Date not null,
  stars NUMBER,
  content CLOB,
  votes NUMBER,
  CONSTRAINT review_pk PRIMARY KEY (review_id),
  CONSTRAINT fk_user_review
    FOREIGN KEY (user_id)
    REFERENCES YelpUser (user_id)
    ON DELETE CASCADE,
  CONSTRAINT fk_business_review
    FOREIGN KEY (business_id)
    REFERENCES Business (business_id)
    ON DELETE CASCADE,
  CONSTRAINT check_stars
    CHECK (stars BETWEEN 1 AND 5)
);
show err;
