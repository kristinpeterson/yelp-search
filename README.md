# Yelp Database Search Application

A web application that searches Yelp data [made available](https://www.yelp.com/dataset_challenge) by [Yelp](http://yelp.com/) for academic purposes. Built for CECS 521 at California State University Long Beach, for more info see Requirements Documentation in the `/docs` directory.

This application was built using the following technologies:

* [Python 2.7](https://www.python.org/download/releases/2.7)
  * [cx_Oracle](https://pypi.python.org/pypi/cx_Oracle/5.0.2): Python Oracle Client
  * [Flask](http://flask.pocoo.org/): Python Web Microframework
* [Oracle 11g Release 2 for Linux](http://www.oracle.com/technetwork/database/enterprise-edition/downloads/112010-linx8664soft-100572.html)
* [Docker](https://www.docker.com/): application container

## Prerequisites

To run this application you need to first install [Docker](https://www.docker.com/products/overview#/install_the_platform), which was used to ease setup of the developement environment and increase portability.

## Setup

Skip to [Build Docker Image](#build-docker-image) if you already have all files downloaded locally, including those under `/data` and `/oracle`.

### Clone the Repo

`git clone https://kristinpeterson@bitbucket.org/kristinpeterson/yelp-search.git`

### Install Git LFS

This repo uses git large file storage for files in `/data` and `/oracle`. Docker build will fail if git-lfs files are not fetched.

To install `git-lfs` on Ubuntu:

1. Add git-lfs to apt-get local:
  `curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.deb.sh | sudo bash`

2. Install git-lfs:
  `sudo apt-get install git-lfs`

### Fetch Git LFS Files

While inside the project root directory:

```
git lfs pull
````

### Build Docker Image

__Make sure Docker is running__

While inside the project root directory:

`docker build -t hw3 .`

## How to populate data:

1. Run container - bash shell
  ```
  docker run -it -e DB_CONNECTION_STRING='kristinpeterson/password@//oracle.cellri4igc5t.us-west-2.rds.amazonaws.com:1521/ORCL' -v '/Users/kristinpeterson/Google Drive/School/CECS 521/HW3:/home/yelp' -w '/home/yelp' hw3 /bin/bash
  ```
2. Run `python populate.py`

## How to run the Flask web app for local usage:

1. Run container in background
  ```
  docker run -d -p 5000:5000 -e DB_CONNECTION_STRING='kristinpeterson/password@//oracle.cellri4igc5t.us-west-2.rds.amazonaws.com:1521/ORCL' -e FLASK_DEBUG='1' -v '/Users/kristinpeterson/Google Drive/School/CECS 521/HW3:/home/yelp' hw3 python app.py
  ```
2. Navigate to `localhost:5000` to view running web app

